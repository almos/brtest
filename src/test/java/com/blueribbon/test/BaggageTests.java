package com.blueribbon.test;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.blueribbon.test.dal.BaggageDal;
import com.blueribbon.test.dal.DestinationDal;
import com.blueribbon.test.dal.InMemoryBaggageDal;
import com.blueribbon.test.dal.InMemoryDestinationDal;
import com.blueribbon.test.entities.DestinationEntity;
import com.blueribbon.test.services.BaggageService;

@RunWith(MockitoJUnitRunner.class)
public class BaggageTests {

	@InjectMocks
    private BaggageService baggageService;
	
	@Spy 
	private BaggageDal baggageDal = new InMemoryBaggageDal();
	
	@Spy
	private DestinationDal destinationDal = new InMemoryDestinationDal();
	
	@Before
	public void init() throws Exception {
		destinationDal.insert(new DestinationEntity(1000));
		destinationDal.insert(new DestinationEntity(1500));
	}
	
	@Test
	public void verifyValidCheckin() throws Exception {
		Assert.assertTrue(baggageService.checkin(1000, UUID.randomUUID()));
	}

	@Test
	public void verifyCheckinWithInvalidDestination() throws Exception {
		Assert.assertFalse(baggageService.checkin(101, UUID.randomUUID()));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void verifyNullDestination() throws Exception {
		Assert.assertFalse(baggageService.checkin(null, UUID.randomUUID()));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void verifyNullBaggage() throws Exception {
		Assert.assertFalse(baggageService.checkin(1000, null));
	}
	
	@Test
	public void verifyMultipleSameBaggages() throws Exception {
		UUID uuid = UUID.randomUUID();
		Assert.assertTrue(baggageService.checkin(1000, uuid));
		Assert.assertFalse(baggageService.checkin(1500, uuid));
	}
	
	@Test
	public void verifyBaggageDal() throws Exception {
		UUID uuid = UUID.randomUUID();
		Assert.assertTrue(baggageService.checkin(1000, uuid));
		Assert.assertNotNull(baggageDal.find(uuid));
	}

}
