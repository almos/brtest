package com.blueribbon.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.blueribbon.test.dal.InMemoryTicketDal;
import com.blueribbon.test.dal.TicketDal;
import com.blueribbon.test.entities.TicketEntity;
import com.blueribbon.test.services.TicketService;

@RunWith(MockitoJUnitRunner.class)
public class TicketTests {

	@InjectMocks
    private TicketService ticketService;
	
	@Spy
	private TicketDal ticketDal = new InMemoryTicketDal();
	
	@Before
	public void init() throws Exception {
		ticketDal.insert(new TicketEntity(100));
		ticketDal.insert(new TicketEntity(200));
		ticketDal.insert(new TicketEntity(300));
	}
	
	@Test
	public void verifyExistingTicket() throws Exception {
		Assert.assertTrue(ticketService.checkTicket(100));
	}

	@Test
	public void verifyInexistingTicket() throws Exception {
		Assert.assertFalse(ticketService.checkTicket(101));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void verifyNullTicket() throws Exception {
		Assert.assertFalse(ticketService.checkTicket(null));
	}
}
