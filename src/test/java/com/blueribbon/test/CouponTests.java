package com.blueribbon.test;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.blueribbon.test.dal.CouponDal;
import com.blueribbon.test.dal.InMemoryCouponDal;
import com.blueribbon.test.entities.CouponEntity;
import com.blueribbon.test.services.CouponService;

@RunWith(MockitoJUnitRunner.class)
public class CouponTests {

	@InjectMocks
    private CouponService couponService;
	
	@Spy
	private CouponDal couponDal = new InMemoryCouponDal();
	
	@Before
	public void init() throws Exception {
		couponDal.insert(new CouponEntity(11, BigDecimal.valueOf(10)));
		couponDal.insert(new CouponEntity(12, BigDecimal.valueOf(50)));
	}
	
	@Test
	public void verifyExistingCoupon() throws Exception {
		Assert.assertTrue(couponService.isValid(11));
	}

	@Test
	public void verifyInexistingCoupon() throws Exception {
		Assert.assertFalse(couponService.isValid(10));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void verifyNullCoupon() throws Exception {
		Assert.assertFalse(couponService.isValid(null));
	}
	
	@Test
	public void verifyValidCouponApplication() throws Exception {
		Assert.assertEquals(BigDecimal.valueOf(50.5), couponService.claim(12, BigDecimal.valueOf(101)));
	}
	
	@Test
	public void verifyInvalidCouponApplication() throws Exception {
		Assert.assertEquals(BigDecimal.valueOf(101), couponService.claim(1, BigDecimal.valueOf(101)));
	}

}
