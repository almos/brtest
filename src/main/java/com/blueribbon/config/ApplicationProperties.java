package com.blueribbon.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationProperties {

	@Value("${app.tickets.generate:10}")
	private int ticketsToGenerate;

	@Value("${app.coupons.generate:10}")
	private int couponsToGenerate;

	@Value("${app.destinations.generate:10}")
	private int DestinationsToGenerate;

	public int getTicketsToGenerate() {
		return ticketsToGenerate;
	}

	public void setTicketsToGenerate(int ticketsToGenerate) {
		this.ticketsToGenerate = ticketsToGenerate;
	}

	public int getCouponsToGenerate() {
		return couponsToGenerate;
	}

	public void setCouponsToGenerate(int couponsToGenerate) {
		this.couponsToGenerate = couponsToGenerate;
	}

	public int getDestinationsToGenerate() {
		return DestinationsToGenerate;
	}

	public void setDestinationsToGenerate(int destinationsToGenerate) {
		DestinationsToGenerate = destinationsToGenerate;
	}

}
