package com.blueribbon.test.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueribbon.test.cache.CallCache;
import com.blueribbon.test.dal.TicketDal;
import com.blueribbon.test.entities.TicketEntity;

/**
 * High level service for manipulating with tickets
 * @author alex.moskvin
 */
@Service
public class TicketService {
	
	@Autowired
	private TicketDal ticketDal;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Checks whether ticket is available
	 * @param ticketId ticket identifier
	 * @return true if ticket with given ID is available and false otherwise 
	 */
	@CallCache
	public boolean checkTicket(Integer ticketId) throws Exception {
	
		logger.info("Checking a ticket {} availability", ticketId);
		
		try {
			TicketEntity e = ticketDal.find(ticketId);
			return e != null;
		} catch (Exception e) {
			logger.error("Error occured while checking availability of ticket {} ", ticketId);
			throw e;
		}
		
	}
}
