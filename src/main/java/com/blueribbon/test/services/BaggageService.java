package com.blueribbon.test.services;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueribbon.test.dal.BaggageDal;
import com.blueribbon.test.dal.DestinationDal;
import com.blueribbon.test.entities.BaggageEntity;
import com.blueribbon.test.entities.DestinationEntity;

/**
 * High level service for manipulating with baggage
 * @author alex.moskvin
 */
@Service
public class BaggageService {

	@Autowired
	private BaggageDal baggageDal;
	
	@Autowired
	private DestinationDal destinationDal;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Checks baggage in
	 * @param destinationId destination identifier
	 * @param baggageId baggage identifier
	 * @return true if baggage is not yet checked in within a valid destination, false otherwise 
	 */
	public boolean checkin(Integer destinationId, UUID baggageId) throws Exception
	{
		logger.info("Checking-in baggage {} wihin a destination {}", baggageId, destinationId);
		
		try {
			DestinationEntity destination = destinationDal.find(destinationId);
			BaggageEntity baggage = baggageDal.find(baggageId);
			
			if (destination == null) {
				logger.error("Cannot check-in baggage, as destination {} doesn't exist", destinationId);
				return false;
			}

			if (baggage != null) {
				logger.error("Cannot check-in baggage {}, as it has been already checked in", baggageId);
				return false;
			}
			
			baggageDal.insert(new BaggageEntity(baggageId));
			
			return true;
			
		} catch (Exception e) {
			logger.error("Error occured while checking in baggage {} within a destination {}", baggageId, destinationId);
			throw e;
		}
	}
	
}
