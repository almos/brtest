package com.blueribbon.test.services;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueribbon.test.dal.CouponDal;
import com.blueribbon.test.entities.CouponEntity;

/**
 * High level service for manipulating with coupons
 * @author alex.moskvin
 */
@Service
public class CouponService {

	@Autowired
	private CouponDal couponDal;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Checks whether given coupon is valid
	 * @param couponId coupon identifier
	 * @return true if coupon with given ID is valid and false otherwise 
	 */
	public boolean isValid(Integer couponId) throws Exception {
		try {
			return couponDal.find(couponId) != null;
		} catch (Exception e) {
			logger.error("Error occured while validating coupon {} ", couponId);
			throw e;
		}
	}
	
	/**
	 * Applies coupon for a price
	 * @param couponId coupon identifier
	 * @param price amount to which coupon needs to be applied
	 * @return result value or source price if coupon is not valid 
	 */
	public BigDecimal claim(Integer couponId, BigDecimal price) throws Exception 
	{
		try {
			CouponEntity coupon = couponDal.find(couponId);
			if (coupon == null)
				return price;
			else
				return price.multiply(coupon.getDiscount())
						.divide(BigDecimal.valueOf(100));
		} catch (Exception e) {
			logger.error("Error occured while applying coupon {}", couponId);
			throw e;
		}
	}
	
}
