package com.blueribbon.test.cache;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

@Aspect
@Component
public class CacheAspect {
	
	/**
	 * Cache based on Guava cache
	 */
	private Cache<String, Object> callCache = CacheBuilder.newBuilder()
		    .maximumSize(1000)
		    .expireAfterAccess(5, TimeUnit.MINUTES)
		    .build();
	
	/**
	 * Logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Handler that intercepts methods that are annotated with {@see CallCache} 
	 */
	@Around("@annotation(CallCache)")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
	    
		String callSignature = jointPointToString(joinPoint);
		Object result = callCache.getIfPresent(callSignature);
		
		if (result != null) {
			logger.debug("Found cached value {} for {}", result, joinPoint.toLongString());
			return result; 
		}
		
		result = joinPoint.proceed();
		callCache.put(callSignature, result);
	    
	    return result;
	}
	
	/**
	 * Creates a string representation of JoinPoint that's depend on method signature and arguments 
	 */
	private String jointPointToString(ProceedingJoinPoint joinPoint)
	{
		StringBuilder result = new StringBuilder();
		result.append(joinPoint.toLongString());
	
		// adding argument values to the signature
		for (Object o : joinPoint.getArgs()) {
			result.append("-").append(o.toString());
		}
		
		return result.toString();
	}
	
}
