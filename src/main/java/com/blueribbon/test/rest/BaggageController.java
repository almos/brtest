package com.blueribbon.test.rest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.blueribbon.test.dto.BaggageDto;
import com.blueribbon.test.services.BaggageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value="Baggage management endpoints")
@RestController
public class BaggageController {

	@Autowired
	private BaggageService baggageService;
	
	@ApiOperation(value = "Provides baggage check in service", response = BaggageDto.class)
	@GetMapping("/baggage/checkin/{destinationId}/{baggageId}")
    public BaggageDto claim(
    		@ApiParam(value = "Destination ID", example="123", required = true) @PathVariable(value="destinationId") Integer destinationId,
    		@ApiParam(value = "Baggage ID (UUID format)", example="51b16964-de97-4d79-8dd1-ac1e42c6029f", required = true) @PathVariable(value="baggageId") UUID baggageId) throws Exception {
		return new BaggageDto(baggageService.checkin(destinationId, baggageId));
    }
	
}
