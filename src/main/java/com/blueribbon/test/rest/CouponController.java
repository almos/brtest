package com.blueribbon.test.rest;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.blueribbon.test.dto.CouponDto;
import com.blueribbon.test.services.CouponService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value="Coupon management endpoints")
@RestController
public class CouponController {
	
	@Autowired
	private CouponService couponService;
	
	@ApiOperation(value = "Provides coupon support", response = CouponDto.class)
	@GetMapping("/coupon/claim/{couponId}")
    public CouponDto claim(
    		@ApiParam(value = "Coupon ID", example="109", required = true) @PathVariable(value="couponId") Integer couponId,
    		@ApiParam(value = "Coupon price", example="99.9", required = true) @RequestParam(value="price") BigDecimal price) throws Exception {
		return new CouponDto(
				couponService.isValid(couponId),
				couponService.claim(couponId, price));
    }
}
