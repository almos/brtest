package com.blueribbon.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.blueribbon.test.dto.TicketDto;
import com.blueribbon.test.services.TicketService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value="Ticket management endpoints")
@RestController
public class TicketController {

	@Autowired
	private TicketService ticketService;
	
	@ApiOperation(value = "Checks if ticket is available", response = TicketDto.class)
	@GetMapping("/ticket/check/{id}")
    public TicketDto claim(
    		@ApiParam(value = "Ticket ID", required = true) @PathVariable(value="id") Integer ticketId) throws Exception {
		return new TicketDto(ticketService.checkTicket(ticketId));
    }
	
}
