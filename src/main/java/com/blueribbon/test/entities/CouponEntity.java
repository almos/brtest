package com.blueribbon.test.entities;

import java.math.BigDecimal;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class CouponEntity {
	private Integer id;
	private BigDecimal discount;
	
	public CouponEntity() {
	}
	
	public CouponEntity(Integer id, BigDecimal discount) {
		this.id = id;
		this.discount = discount;
	}
	
	public BigDecimal getDiscount() {
		return discount;
	}
	
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final CouponEntity other = (CouponEntity) obj;
        return Objects.equal(this.id, other.id);
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
	            .add("id", id)
	            .add("discount", discount)
	            .toString();
	}
}
