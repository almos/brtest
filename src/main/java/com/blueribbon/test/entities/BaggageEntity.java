package com.blueribbon.test.entities;

import java.util.UUID;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class BaggageEntity {
	
	private UUID id;
	
	public BaggageEntity() {
	}

	public BaggageEntity(UUID id) {
		this.id = id;
	}
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final BaggageEntity other = (BaggageEntity) obj;
        return Objects.equal(this.id, other.id);
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
	            .add("id", id)
	            .toString();
	}
}
