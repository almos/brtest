package com.blueribbon.test.entities;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class DestinationEntity {
	private Integer id;

	public DestinationEntity() {
	}

	public DestinationEntity(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final DestinationEntity other = (DestinationEntity) obj;
        return Objects.equal(this.id, other.id);
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
	            .add("id", id)
	            .toString();
	}
}
