package com.blueribbon.test.dal;

import java.util.UUID;

import org.springframework.stereotype.Component;

import com.blueribbon.test.entities.BaggageEntity;

@Component
public class InMemoryBaggageDal extends BaseInMemoryDal<BaggageEntity, UUID> implements BaggageDal {

	@Override
	public void insert(BaggageEntity baggage) throws Exception {
		insertOne(baggage, baggage.getId());
	}

	@Override
	public BaggageEntity find(UUID baggageId) throws Exception {
		return findOne(baggageId);
	}

}
