package com.blueribbon.test.dal;

import com.blueribbon.test.entities.CouponEntity;

/**
 * Data access layer class for {@link CouponEntity} entities 
 * @author alex.moskvin
 */
public interface CouponDal {
	void insert(CouponEntity coupon) throws Exception;
	CouponEntity find(Integer couponId) throws Exception;
}
