package com.blueribbon.test.dal;

import com.blueribbon.test.entities.DestinationEntity;

/**
 * Data access layer class for {@link DestinationEntity} entities 
 * @author alex.moskvin
 */
public interface DestinationDal {
	void insert(DestinationEntity destination) throws Exception;
	DestinationEntity find(Integer destinationId) throws Exception;
}
