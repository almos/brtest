package com.blueribbon.test.dal;

import java.util.UUID;

import com.blueribbon.test.entities.BaggageEntity;

/**
 * Data access layer class for {@link BaggageEntity} entities 
 * @author alex.moskvin
 */
public interface BaggageDal {
	void insert(BaggageEntity baggage) throws Exception;
	BaggageEntity find(UUID baggageId) throws Exception;
}
