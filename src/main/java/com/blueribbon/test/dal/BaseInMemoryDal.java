package com.blueribbon.test.dal;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Base implementation for in-memory storage DALs 
 * @author alex.moskvin
 */
public class BaseInMemoryDal<E, K> {
	
	private Map<K, E> entities = new ConcurrentHashMap<>();
	private ReadWriteLock lock = new ReentrantReadWriteLock();
	
	protected E findOne(K key) {
		
		if (key == null)
			throw new IllegalArgumentException("Key may not be null");
		
		lock.readLock().lock();
		try {
			return entities.get(key);
		} finally {
			lock.readLock().unlock();
		}
	}
	
	protected void insertOne(E entity, K key) {
		
		if (key == null || entity == null)
			throw new IllegalArgumentException("Key/object may not be null");
		
		lock.writeLock().lock();
		try {
			entities.put(key, entity);
		} finally {
			lock.writeLock().unlock();
		}
	}
	
	protected void deleteOne(K key) {
		
		if (key == null)
			throw new IllegalArgumentException("Key may not be null");;
		
		lock.writeLock().lock();
		try {
			entities.remove(key);
		} finally {
			lock.writeLock().unlock();
		}
	}
	
	protected void updateOne(E entity, K key) {
		insertOne(entity, key);
	} 
}
