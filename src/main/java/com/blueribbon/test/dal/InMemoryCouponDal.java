package com.blueribbon.test.dal;

import org.springframework.stereotype.Component;

import com.blueribbon.test.entities.CouponEntity;

@Component
public class InMemoryCouponDal extends BaseInMemoryDal<CouponEntity, Integer> implements CouponDal {

	@Override
	public void insert(CouponEntity coupon) throws Exception {
		insertOne(coupon, coupon.getId());
	}

	@Override
	public CouponEntity find(Integer couponId) throws Exception {
		return findOne(couponId);
	}

}
