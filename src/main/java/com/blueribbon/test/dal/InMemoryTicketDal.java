package com.blueribbon.test.dal;

import org.springframework.stereotype.Component;

import com.blueribbon.test.entities.TicketEntity;

@Component
public class InMemoryTicketDal extends BaseInMemoryDal<TicketEntity, Integer> implements TicketDal {

	@Override
	public void insert(TicketEntity ticket) throws Exception {
		insertOne(ticket, ticket.getId());
	}

	@Override
	public TicketEntity find(Integer ticketId) throws Exception {
		return findOne(ticketId);
	}
}
