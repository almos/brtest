package com.blueribbon.test.dal;

import com.blueribbon.test.entities.TicketEntity;

/**
 * Data access layer class for {@link TicketEntity} entities 
 * @author alex.moskvin
 */
public interface TicketDal {
	void insert(TicketEntity ticket) throws Exception;
	TicketEntity find(Integer ticketId) throws Exception;
}
