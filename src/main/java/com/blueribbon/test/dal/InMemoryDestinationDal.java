package com.blueribbon.test.dal;

import org.springframework.stereotype.Component;

import com.blueribbon.test.entities.DestinationEntity;

@Component
public class InMemoryDestinationDal extends BaseInMemoryDal<DestinationEntity, Integer> implements DestinationDal {

	@Override
	public void insert(DestinationEntity destination) throws Exception {
		insertOne(destination, destination.getId());
	}

	@Override
	public DestinationEntity find(Integer destinationId) throws Exception {
		return findOne(destinationId);
	}

}
