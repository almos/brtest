package com.blueribbon.test.dto;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Coupon entity details")
public class CouponDto {
	
	@ApiModelProperty(notes = "Indicates whether coupon ID supplied by client is valid", example="true")	
	private Boolean isValid;
	
	@ApiModelProperty(notes = "Final price for given costs", example="80.90")
	private BigDecimal finalPrice;
	
	public CouponDto() {
	}

	public CouponDto(Boolean isValid, BigDecimal finalPrice) {
		this.isValid = isValid;
		this.finalPrice = finalPrice;
	}
	
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	public BigDecimal getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}
}
