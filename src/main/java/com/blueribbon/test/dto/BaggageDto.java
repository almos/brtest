package com.blueribbon.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Baggage entity details")
public class BaggageDto {
	
	@ApiModelProperty(notes = "Indicates whether checking has succeeded", example="true")
	private Boolean checkinSucceeded;

	public BaggageDto() {
	}

	public BaggageDto(Boolean checkinSucceeded) {
		this.checkinSucceeded = checkinSucceeded;
	}
	
	public Boolean getCheckinSucceeded() {
		return checkinSucceeded;
	}

	public void setCheckinSucceeded(Boolean checkinSucceeded) {
		this.checkinSucceeded = checkinSucceeded;
	}
}
