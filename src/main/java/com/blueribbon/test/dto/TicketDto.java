package com.blueribbon.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "CoupTicketon entity details")
public class TicketDto {
	
	@ApiModelProperty(notes = "Indicates whether ticket is available", example="true")
	private Boolean isAvailable;
	
	public TicketDto() {
	}

	public TicketDto(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public Boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
}
