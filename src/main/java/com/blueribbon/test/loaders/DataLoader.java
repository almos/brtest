package com.blueribbon.test.loaders;

import java.math.BigDecimal;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.blueribbon.config.ApplicationProperties;
import com.blueribbon.test.dal.CouponDal;
import com.blueribbon.test.dal.DestinationDal;
import com.blueribbon.test.dal.TicketDal;
import com.blueribbon.test.entities.CouponEntity;
import com.blueribbon.test.entities.DestinationEntity;
import com.blueribbon.test.entities.TicketEntity;

@Component
public class DataLoader {

	@Autowired
	private DestinationDal destinationDal;
	
	@Autowired
	private CouponDal couponDal;
	
	@Autowired
	private TicketDal ticketDal;
	
	@Autowired
	private ApplicationProperties properties;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@EventListener(ApplicationReadyEvent.class)
	public void initializeData() throws Exception {
		generateTickets();
		generateCoupons();
		generateDestinations();
	}

	private void generateTickets() throws Exception {
		Random rand = new Random();
		logger.info("Going to generate {} Ticket records", properties.getTicketsToGenerate());
		for (int i = 0 ; i < properties.getTicketsToGenerate() ; i++) {
			TicketEntity e = new TicketEntity(rand.nextInt(100));
			ticketDal.insert(e);
			logger.info(" -> ticket {} generated", e);
		}
	}

	private void generateCoupons() throws Exception {
		Random rand = new Random();
		logger.info("Going to generate {} Coupon records", properties.getCouponsToGenerate());
		for (int i = 0 ; i < properties.getCouponsToGenerate() ; i++) {
			CouponEntity e = new CouponEntity(rand.nextInt(1000), new BigDecimal(rand.nextInt(100)));
			couponDal.insert(e);
			logger.info(" -> coupon {} generated", e);
		}
	}
	
	private void generateDestinations() throws Exception {
		Random rand = new Random();
		logger.info("Going to generate {} Destination records", properties.getDestinationsToGenerate());
		for (int i = 0 ; i < properties.getDestinationsToGenerate() ; i++) {
			DestinationEntity e = new DestinationEntity(rand.nextInt(100));
			destinationDal.insert(e);
			logger.info(" -> destination {} generated", e);
		}
	}

}
