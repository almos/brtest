## Prerequisites

- JDK 1.8.x
- Maven 3.2.x+

## External dependencies

Deliverable is self-contained, no external software is required

## Deliverables

- Code
- Swagger documentation
- Unit tests

## Execution

Use maven

```bash
mvn spring-boot:run
```

After startup server will show which entities it has generated

```
2019-08-19 01:45:29.409  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   : Going to generate 10 Ticket records
2019-08-19 01:45:29.409  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=2} generated
2019-08-19 01:45:29.410  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=7} generated
2019-08-19 01:45:29.410  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=95} generated
2019-08-19 01:45:29.410  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=45} generated
2019-08-19 01:45:29.410  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=70} generated
2019-08-19 01:45:29.411  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=45} generated
2019-08-19 01:45:29.411  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=50} generated
2019-08-19 01:45:29.411  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=1} generated
2019-08-19 01:45:29.411  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=38} generated
2019-08-19 01:45:29.411  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> ticket TicketEntity{id=33} generated
2019-08-19 01:45:29.411  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   : Going to generate 15 Coupon records
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=950, discount=94} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=896, discount=69} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=375, discount=3} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=513, discount=85} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=847, discount=81} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=662, discount=89} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=372, discount=24} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=398, discount=33} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=899, discount=44} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=936, discount=37} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=745, discount=23} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=885, discount=72} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=407, discount=76} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=339, discount=15} generated
2019-08-19 01:45:29.412  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> coupon CouponEntity{id=556, discount=29} generated
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   : Going to generate 20 Destination records
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=72} generated
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=89} generated
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=27} generated
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=35} generated
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=65} generated
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=45} generated
2019-08-19 01:45:29.413  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=65} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=76} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=54} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=1} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=57} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=35} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=3} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=55} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=75} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=82} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=9} generated
2019-08-19 01:45:29.414  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=61} generated
2019-08-19 01:45:29.415  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=95} generated
2019-08-19 01:45:29.415  INFO 37732 --- [           main] com.blueribbon.test.loaders.DataLoader   :  -> destination DestinationEntity{id=69} generated
```

After startup endpoints may be tested using Swagger UI: http://localhost:8080/swagger-ui.html